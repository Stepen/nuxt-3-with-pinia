// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  typescript: { shim: false },
  modules: ["@nuxtjs/tailwindcss", "@pinia/nuxt"],
  app: {
    head: {
      title: "Learn Nuxt 3 with Pinia",
      meta: [{ name: "description", content: "Everything about nuxt 3 and pinia" }],
      link: [{ rel: "stylesheet", href: "https://fonts.googleapis.com/icon?family=Material+Icons" }],
    },
  },
  runtimeConfig: {
    currencyKey: process.env.CURRENCY_API_KEY,
    public: {},
  },
});
