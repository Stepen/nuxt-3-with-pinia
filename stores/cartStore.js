import { defineStore } from "pinia";

export const useCartStore = defineStore("cart", () => {
  const cart = ref([]);

  const cartTotal = computed(() => {
    return cart.value.reduce((total, item) => {
      return total + item.price * item.quantity;
    }, 0);
  });

  const numberofProducts = computed(() => {
    return cart.value.reduce((total, item) => {
      return total + item.quantity;
    }, 0);
  });

  async function getCart() {
    const data = await $fetch("http://localhost:4000/cart");
    cart.value = data;
  }

  async function deleteFromCart(product) {
    cart.value = cart.value.filter((p) => p.id != product.id);

    await $fetch(`http://localhost:4000/cart/${product.id}`, {
      method: "delete",
    });
  }

  async function incQuantity(product) {
    let updatedProduct;

    cart.value = cart.value.map((p) => {
      if (p.id === product.id) {
        p.quantity++;
        updatedProduct = p;
      }
      return p;
    });

    await $fetch(`http://localhost:4000/cart/${product.id}`, {
      method: "put",
      body: JSON.stringify(updatedProduct),
    });
  }

  async function decQuantity(product) {
    let updatedProduct;

    cart.value = cart.value.map((p) => {
      if (p.id === product.id) {
        p.quantity--;
        updatedProduct = p;
      }
      return p;
    });
    console.log("asd", updatedProduct.quantity > 0);
    if (updatedProduct.quantity > 0) {
      await $fetch(`http://localhost:4000/cart/${product.id}`, {
        method: "put",
        body: JSON.stringify(updatedProduct),
      });
    } else {
      deleteFromCart(updatedProduct);
    }
  }

  async function addToCart(product) {
    const exists = cart.value.find((p) => p.id === product.id);

    if (exists) {
      incQuantity(product);
    }

    if (!exists) {
      cart.value.push({ ...product, quantity: 1 });

      await $fetch("http://localhost:4000/cart", {
        method: "post",
        body: JSON.stringify({ ...product, quantity: 1 }),
      });
    }
  }

  return {
    cart,
    cartTotal,
    numberofProducts,
    getCart,
    deleteFromCart,
    incQuantity,
    decQuantity,
    addToCart,
  };
});
